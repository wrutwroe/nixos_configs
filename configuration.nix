# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
#      ./vim.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  # Bigger console font
#  boot.loader.systemd-boot.consoleMode = "2";
#  boot.earlyVconsoleSetup = true;
  # Prohibits gaining root access by passing init=/bin/sh as a kernel parameter
  boot.loader.systemd-boot.editor = false;
  boot.loader.efi.canTouchEfiVariables = true;

  i18n = {
    defaultLocale = "en_US.UTF-8";
    supportedLocales = [ "en_US.UTF-8/UTF-8" ];
  };

  console = {
    earlySetup = true;
    font = "ter-i16b";
    packages = with pkgs; [ terminus_font ];
    keyMap = "us";
  };

  fonts = {
    enableDefaultFonts = true;
    fontDir.enable = true;
    enableGhostscriptFonts = true;
    fonts = with pkgs; [
      cascadia-code
      corefonts
      roboto
      (nerdfonts.override { fonts = [ "CodeNewRoman" "FiraCode" "FiraMono" "Hack" "Hermit" "JetBrainsMono" "OpenDyslexic" "VictorMono" "Ubuntu" "UbuntuMono" ]; }) 
    ];
  };

  # Enable microcode updates for Intel CPU
  hardware.cpu.intel.updateMicrocode = true;
  # Enable Kernel same-page merging
  hardware.ksm.enable = true;

  # Enable all the firmware
  hardware.enableAllFirmware = true;
  hardware.enableRedistributableFirmware = true;

  # Enable OpenGL drivers
  hardware.opengl.enable = true;
  hardware.opengl.extraPackages = with pkgs; [
    vaapiIntel
    vaapiVdpau
    libvdpau-va-gl
  ];

  # A DBus service that allows applications to update firmware
  services.fwupd.enable = true;

  # Check S.M.A.R.T status of all disks and notify in case of errors
  services.smartd = {
    enable = true;
    # Monitor all devices connected to the machine at the time it's being started
    autodetect = true;
    notifications = {
      x11.enable = if config.services.xserver.enable then true else false;
      wall.enable = true; # send wall notifications to all users
    };
  };

  virtualisation.libvirtd.enable = true;
  programs.dconf.enable = true;

  # Set your time zone.
  time.timeZone = "America/Denver";

  # sudo
  security = {
    sudo = {
      enable = true;
      wheelNeedsPassword = true;
    };
  };

  networking.hostName = "kala";
  # networking.wireless.enable = true;
  networking.networkmanager.enable = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the Plasma 5 Desktop Environment.
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.displayManager.sddm.settings.Wayland.SessionDir = "${pkgs.plasma5Packages.plasma-workspace}/share/wayland-sessions";
  services.xserver.desktopManager.plasma5.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.mike = {
    initialPassword = "pickle123";
    isNormalUser = true;
    extraGroups = [ "wheel" "libvirtd" ]; # Enable ‘sudo’ for the user.
  #   packages = with pkgs; [
  #     firefox
  #   ];
  };

  ## PACKAGES
  ## List packages installed in system profile. To search, run:
  ## $ nix search wget

nixpkgs.config = {
    allowUnfree = true;
  };

  environment.systemPackages = with pkgs; [
    vim neovim ark mpv vlc firefox-wayland vivaldi kate krita git wget openvpn exa hplip p7zip kitty tmux exa lm_sensors acpi mc virt-manager
    gcc pkgconfig automake gnumake rustup 
    python3 yapf python310Packages.flake8 python310Packages.pylint
    ansible terraform vagrant
    vim-vint ripgrep wl-clipboard neovide zip unzip
    texlive.combined.scheme-medium biber

  fishPlugins.done fishPlugins.fzf-fish fishPlugins.forgit fishPlugins.hydro fzf fishPlugins.pure grc

  ];

  programs.fish.enable = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  ## SERVICES

  # OpenSSH
  services.openssh = {
    enable = true;
    settings.PermitRootLogin = "no";
    settings.PasswordAuthentication = false;
  };

  # firewall
  networking.firewall = {
    enable = true;
    allowPing = true;
    allowedTCPPorts = [];
    allowedUDPPorts = [];
    logRefusedConnections = true;
#    checkReversePath = false; # for libvirtd
  };

  services.openvpn.servers = {
    vpn1udp  = { config = '' config /home/mike/VPN/vpn1udp.ovpn ''; autoStart = false; updateResolvConf = true; };
    vpn1tcp  = { config = '' config /home/mike/VPN/vpn1tcp.ovpn ''; autoStart = false; updateResolvConf = true; };
    vpn2udp  = { config = '' config /home/mike/VPN/vpn2udp.ovpn ''; autoStart = false; updateResolvConf = true; };
    vpn2tcp  = { config = '' config /home/mike/VPN/vpn2tcp.ovpn ''; autoStart = false; updateResolvConf = true; };
  };

  # Network usage statistics
  services.vnstat.enable = true;

  # Cleanup
  nix = {
    settings = {
      sandbox = true;
      auto-optimise-store = true;
      allowed-users = [ "@wheel" ];
      trusted-users = [ "@wheel" ];
    };
    extraOptions = ''
      experimental-features = nix-command flakes
      keep-outputs = true
      keep-derivations = true
    '';
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 14d --max-freed $((64 * 1024**3))";
    };
    optimise = {
      automatic = true;
      dates = [ "weekly" ];
    };
  };



  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

}

